/**
 * The main class that starts fetches the configuration and starts up the SpringBoot app part and the port listener on the port found in config
 */

package httpServer;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.example.demo.AppRun;
import com.example.demo.SingletonConnection;

import config.Configuration;
import config.ConfigurationManager;
import core.ServerListenerThread;

/**
 * 
 * Driver class for http server
 *
 */
@SpringBootApplication
@EnableScheduling
public class HttpServer {

	private final static Logger LOGGER = LoggerFactory.getLogger(HttpServer.class);
	static final String topicExchangeName = "sensorQueueExchange";
	static final String queueName = "sensorQueue";

	@Bean
	public Queue queue() {
		return new Queue(queueName);
	}

	@Bean
	public TopicExchange exchange() {
		return new TopicExchange(topicExchangeName);
	}

	@Bean
	public Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with("routing");
	}

	public static void main(String[] args) {

		//LOGGER.info("Server starting...");

		SingletonConnection conn = SingletonConnection.getInstance();
		//LOGGER.info("Cassandra instance has been made.");

		SpringApplication.run(AppRun.class, args);

		ConfigurationManager.getInstance().loadConfigurationFile("http.json");

		Configuration configuration = ConfigurationManager.getInstance().getCurrentConfiguration();

		//LOGGER.info("using port: " + configuration.getPort());
		//LOGGER.info("Using webroot: " + configuration.getWebroot());

		try {
			ServerListenerThread serverListenerThread = new ServerListenerThread(configuration.getPort(),
					configuration.getWebroot());
			serverListenerThread.start();
			//LOGGER.info("Server is waiting for a connection..");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
