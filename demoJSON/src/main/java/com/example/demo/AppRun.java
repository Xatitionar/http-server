/**
 * 
 * @author student

 * DemoJsonApplication starts the .run method that enables Spring boot. The full app starts from Httpserver.java, 
 * this is for testing Spring Boot part of the application.
 *
 */
package com.example.demo;

import java.io.IOException;

import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;


@SpringBootApplication
@EnableScheduling
public class AppRun {

  

//	  @Bean
//	 public TopicExchange exchange() {
//	    return new TopicExchange(topicExchangeName);
//	  }
//
//	  @Bean
//	  public Binding binding(Queue queue, TopicExchange exchange) {
//	    return BindingBuilder.bind(queue).to(exchange).with(routingKey);
//	  }

	public static void main(String[] args) throws IOException, TimeoutException {
		  
		SpringApplication.run(AppRun.class, args);
		
	}


}
