/**
 * 
 * @author student
 *JSONListener listens to the dedicated QUEUE (sensorQueue) and when a sensor sends the data to that queue it triggers a callback function. 
  * @RabbitListener is used for that queue. Future release will expand for more sensors.
  * @param sensorTemperature is a substring from the message that represents the temperature in celsius.
  * @param message encapsulates the data that is sent from the sensor. 
  * @throws IOException
  * @throws TimeoutException
  */
package com.example.demo;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import util.ParseSensorInfo;
import util.SensorInfo;


@Service
public class JSONListener {
	private static final Logger log = LoggerFactory.getLogger(JSONListener.class);
 
	public final static String queueName = "sensorQueue";
		
	@Bean
	 public Queue queue() {
	    return new Queue(queueName);
	  }

	@RabbitListener(queues = queueName)
	public void consumeMSG(String message) throws IOException, TimeoutException {
			
		JSONSensor jsonSensor = new JSONSensor();
		jsonSensor.setId(ParseJSONSensor.parseID(message));
		jsonSensor.setTemp(ParseJSONSensor.parseTemp(message));
		jsonSensor.setTimestamp(new Timestamp(System.currentTimeMillis()));
		
		SensorInfo sensorInfo = new SensorInfo();
		sensorInfo.setSensor_MAC(ParseSensorInfo.parseMAC(message));
		sensorInfo.setSensor_ID(ParseSensorInfo.parseID(message));
		sensorInfo.setSensor_IP(ParseSensorInfo.parseIP(message));
		
		JSONSender.sendMessage(jsonSensor, sensorInfo);

	}
 
}
