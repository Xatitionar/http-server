/**
 * SingletonConnection is a class used for connecting to the cassandraDB.
 * @param cqlSession is an instance of the CqlSession interface for connection pooling and CQL queries.
 * @author student
 *
 */
package com.example.demo;

import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.CassandraTemplate;

import com.datastax.driver.core.PoolingOptions;
import com.datastax.oss.driver.api.core.CqlSession;

public class SingletonConnection {
	private CqlSession cqlSession = CqlSession.builder().withKeyspace("sensors").build();
	private CassandraOperations template = new CassandraTemplate(cqlSession);
		
	public CassandraOperations getTemplate() {
		return template;
	}

	public void setTemplate(CassandraOperations template) {
		this.template = template;
	}

	private static SingletonConnection mySIngletonConnection;
	
	private SingletonConnection() {
		
	}
	
	public static SingletonConnection getInstance() {
		if(mySIngletonConnection == null) {
			mySIngletonConnection = new SingletonConnection();
		}
		return mySIngletonConnection;
	}
	
	
}
