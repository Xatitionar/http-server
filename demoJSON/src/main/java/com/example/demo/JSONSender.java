/**
 * JSONSender is used for inserting received data from the sensor along with other parameters to the Cassandra DB. 
 * @param rabbitTemplate is a instance of RabbitTemplate that simplifies RabbitMQ access.
 * @author student
  * sendMessage makes a connection the the database, reads the temperature value from the queue and inserts it in the database. 
  * CQL query is made using CassandraOperations template and the .insert method.
 */
package com.example.demo;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.stereotype.Service;

import util.SensorInfo;

@Service
public class JSONSender {
    private static final Logger log = LoggerFactory.getLogger(JSONSender.class);
    public static RabbitTemplate rabbitTemplate;

    public JSONSender(final RabbitTemplate rabbitTemplate) {
        JSONSender.rabbitTemplate = rabbitTemplate;
    }
        
    static public void sendMessage(JSONSensor sensor, SensorInfo sensorInfo) {
   	
    	SingletonConnection conn = SingletonConnection.getInstance();
    	CassandraOperations template = conn.getTemplate();
    	
    	List<SensorInfo> sensorExists = template.select("SELECT * FROM sensor_info WHERE sensor_mac = '" + sensorInfo.getSensor_MAC() + "' ALLOW FILTERING", SensorInfo.class);
    	List<SensorInfo> listOfAllSensors = template.select("SELECT * FROM sensor_info", SensorInfo.class);
    	
    	int maxSensorNumber = 0;
    	if(!listOfAllSensors.isEmpty()) {
    		for(SensorInfo sens : listOfAllSensors) {
    			if(maxSensorNumber < sens.getSensor_Number()) {
    				maxSensorNumber = sens.getSensor_Number();
    			}
    		}	
    	}
    	
    	sensorInfo.setSensor_Number(maxSensorNumber + 1);  	
    	
    	if(sensorExists.isEmpty()) {
    		template.insert(sensorInfo);
    	}
    	
    	template.insert(sensor);
    }
}
