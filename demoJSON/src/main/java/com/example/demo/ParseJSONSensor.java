package com.example.demo;

import java.sql.Timestamp;
import java.util.UUID;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParseJSONSensor {
	private static final Logger LOGGER = LoggerFactory.getLogger(ParseJSONSensor.class);
	
	private static JSONObject returnJSONObj(String body) {
		JSONParser jsonReader = new JSONParser();
		JSONObject JSONObj = null;
		try {
			JSONObj = (JSONObject) jsonReader.parse(body);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return JSONObj;
	}
	
	public static UUID parseID(String body) {
		JSONObject jsonObj = ParseJSONSensor.returnJSONObj(body);
		return UUID.fromString(jsonObj.get("ID").toString());
	}
	
	public static float parseTemp(String body) {
		JSONObject jsonObj = ParseJSONSensor.returnJSONObj(body);
		double number = (double) jsonObj.get("temperature");
		float floatNumber = (float) number;
		return floatNumber;
	}
	
	public static Timestamp parseTimestamp(String body) {
		JSONObject jsonObj = ParseJSONSensor.returnJSONObj(body);
		return (Timestamp) jsonObj.get("timestamp");
	}
}
