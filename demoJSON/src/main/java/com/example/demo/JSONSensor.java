/**
 * @Table gets the name of the table in the designated keyspace. 
 * The constructor of this class is equal to the table in the newsensorvalues table.
 *
 * @author student
 * @method toString is an overridden method that represents JSON format used for sending data from the DB with HTTP requests.
 */
package com.example.demo;

import java.sql.Timestamp;
import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("sensor_values")
public class JSONSensor {

	@PrimaryKey
	private UUID sensor_id;
	private float sensor_temp;
	private Timestamp sensor_timestamp;

	public JSONSensor() {
		this.sensor_id = UUID.fromString("00000000-0000-0000-0000-000000000000");
		this.sensor_temp = 0;
		this.sensor_timestamp = new Timestamp(System.currentTimeMillis());
	}
	
	public JSONSensor(UUID sensor_id, float sensor_temp, Timestamp sensor_timestamp) {
		this.sensor_id = sensor_id;
		this.sensor_temp = sensor_temp;
		this.sensor_timestamp = sensor_timestamp;
	}

	public UUID getId() {
		return sensor_id;
	}

	public double getTemp() {
		return sensor_temp;
	}

	public Timestamp getTs() {
		return sensor_timestamp;
	}

	public void setId(UUID id) {
		this.sensor_id = id;
	}

	public void setTemp(float temp) {
		this.sensor_temp = temp;
	}

	public void setTimestamp(Timestamp timestamp) {
		this.sensor_timestamp = timestamp;
	}

	@Override
	public String toString() {
		return String.format(
				"{\n \"sensor_id\" : \"%2$s\",\n \"sensor_temp\" : %3$f,\n \"sensor_timestamp\" : \"%4$s\" \n}",
				getId(), getTemp(), getTs().toGMTString());
	}
}