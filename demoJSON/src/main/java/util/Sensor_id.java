package util;

import java.util.UUID;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;

public class Sensor_id {
	@PrimaryKey private final UUID sensor_id;
	
	public Sensor_id(UUID sensor_id) {
		this.sensor_id = sensor_id;
	}

	public UUID getSensor_id() {
		return sensor_id;
	}
	
	@Override
	public String toString() {
		return String.format("%1$f", getSensor_id());
	}
}
