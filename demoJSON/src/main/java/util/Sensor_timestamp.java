/**
 * Class that represents sensor timestamp - needed as a separate class for SELECT function on Cassandra database
 */

package util;

import java.sql.Timestamp;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;

public class Sensor_timestamp {
	@PrimaryKey private final Timestamp sensor_timestamp;
	
	public Sensor_timestamp(Timestamp sensor_timestamp) {
		this.sensor_timestamp = sensor_timestamp;
	}

	public Timestamp getSensor_timestamp() {
		return sensor_timestamp;
	}
	
	public long convertToMilis() {
		Timestamp temp = getSensor_timestamp();
		
		return temp.getTime();
	}
	
	@Override
	public String toString() {
		return String.format("%1$s", getSensor_timestamp());
	}
}
