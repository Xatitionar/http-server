/**
 * Class that represents sensor temperature - needed as a separate class for SELECT function on Cassandra database
 */

package util;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;

public class Sensor_temp {
	@PrimaryKey private final float sensor_temp;
	
	public Sensor_temp(float sensor_temp) {
		this.sensor_temp = sensor_temp;
	}

	public float getSensor_temp() {
		return sensor_temp;
	}
	
	@Override
	public String toString() {
		return String.format("%1$f", getSensor_temp());
	}
}
