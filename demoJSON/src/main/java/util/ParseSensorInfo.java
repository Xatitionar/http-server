/**
 * Class used to parse sensor information values from a json in /insert query to be inserted into Cassandra database 
 */

package util;

import java.util.UUID;

import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

public class ParseSensorInfo {

	public static SensorInfo parse(String body) throws ParseException {
		SensorInfo sensor = new SensorInfo();

		sensor.setSensor_ID(ParseSensorInfo.parseID(body));
		sensor.setSensor_MAC(ParseSensorInfo.parseMAC(body));
		sensor.setSensor_IP(ParseSensorInfo.parseIP(body));
		sensor.setSensor_Location(ParseSensorInfo.parseLocation(body));
		sensor.setSensor_Description(ParseSensorInfo.parseDescription(body));

		return sensor;
	}

	private static JSONObject returnJSONObj(String body) {
		JSONParser jsonReader = new JSONParser();
		JSONObject JSONObj = null;
		try {
			JSONObj = (JSONObject) jsonReader.parse(body);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return JSONObj;
	}

	public static UUID parseID(String body) {
		JSONObject JSONObj = ParseSensorInfo.returnJSONObj(body);
		if(body.contains("ID")) {
			return UUID.fromString(JSONObj.get("ID").toString());
		}else {
			return UUID.fromString(JSONObj.get("sensor_id").toString());
		}
	}

	public static String parseMAC(String body) {
		JSONObject JSONObj = ParseSensorInfo.returnJSONObj(body);
		if(body.contains("MAC")) {
			return JSONObj.get("MAC").toString();	
		}else {
			return JSONObj.get("sensor_mac").toString();
		}
	}

	public static String parseIP(String body) {
		JSONObject JSONObj = ParseSensorInfo.returnJSONObj(body);
		if(body.contains("IP")) {
			return JSONObj.get("IP").toString();
		}else {
			return JSONObj.get("sensor_ip").toString();			
		}
	}

	public static String parseLocation(String body) {
		JSONObject JSONObj = ParseSensorInfo.returnJSONObj(body);
		return JSONObj.get("sensor_location").toString();
	}

	public static String parseDescription(String body) {
		JSONObject JSONObj = ParseSensorInfo.returnJSONObj(body);
		return JSONObj.get("sensor_description").toString();
	}

}
