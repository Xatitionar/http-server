/**
 * Class used to parse specific parts of the http request body to easily get requested id of the sensor and timeframe that needs to be sent back
 */

package util;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParsedRequestJson {
	private final static Logger LOGGER = LoggerFactory.getLogger(ParsedRequestJson.class);
	
	public static String prepareRequestBody(String body) {
		String preparedBody;
		
		boolean check = true;
		int count = 0;
		int positionOfQueryEnd = 0;
		
		while(check) {
			if(body.indexOf("ID", positionOfQueryEnd) != -1) {
				count++;
				positionOfQueryEnd = body.indexOf("ID", positionOfQueryEnd) + 2;
			}else {
				check = false;
			}
		}	
		positionOfQueryEnd += 63;
		
		preparedBody = body.substring(body.indexOf("{\"ID"), positionOfQueryEnd).replaceAll("[{}]", "").replace("\"data\":", "");
		preparedBody = "{" + preparedBody + "}";
		
		return preparedBody;
	}

	public static List<UUID> parseID(String body) throws ParseException {
		JSONParser jsonReader = new JSONParser();
		
		boolean check = true;
		
		List<UUID> id = new ArrayList<UUID>(); 
		
		if(!(body.contains("ID"))) {
			id.add(UUID.fromString("00000000-0000-0000-0000-000000000000"));
			return id;
		}
			
		while(check) {
			if(body.indexOf("\"ID\"", 0) != -1) {
				//parse the body and collect the ID value
				JSONObject JSONObj = (JSONObject)jsonReader.parse(body);
				id.add(UUID.fromString(JSONObj.get("ID").toString()));

				//remove collected "ID"
				body = body.replace("\"ID\":\"" + JSONObj.get("ID").toString() + "\"", "");
				
				//stitch up a new body begining and next "ID"
				if(body.contains(("\"ID\""))){
					body = "{" + body.substring(body.indexOf("\"ID\""), body.length());					
				}
			}else {
				check = false;
			}
		}	
				
		return id;
	}
	
	public static String parseTime(String body) throws ParseException {
		JSONParser jsonReader = new JSONParser();
		JSONObject JSONObj = (JSONObject)jsonReader.parse(body);
		
		String time = "";
		
		if(!(body.contains("time"))) return time;
		
		time = JSONObj.get("time").toString();
		
		return time;
	}
	
}
