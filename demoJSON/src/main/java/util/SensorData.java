/**
 * Class that represents all the values that need to be read from the json inside http request body on /insert query and inserted into Cassandra database
 */

package util;

import java.sql.Timestamp;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("sensor_info")
public class SensorData {
	private final static Logger LOGGER = LoggerFactory.getLogger(SensorData.class);
	
	@PrimaryKey UUID sensor_id;
	private float sensor_temp;
	private Timestamp sensor_timestamp;
	
	public UUID getSensor_id() {
		return this.sensor_id;
	}
	public void setSensor_id(UUID sensor_id) {
		this.sensor_id = sensor_id;
	}
	public float getSensor_temp() {
		return this.sensor_temp;
	}
	public void setSensor_temp(float sensor_temp) {
		this.sensor_temp = sensor_temp;
	}
	public Timestamp getSensor_timestamp() {
		return sensor_timestamp;
	}
	public void setSensor_timestamp(Timestamp sensor_timestamp) {
		this.sensor_timestamp = sensor_timestamp;
	}
	
	public SensorData() {
		this.sensor_id = UUID.fromString("00000000-0000-0000-0000-000000000000");
		this.sensor_temp = (float) 25.00;
		this.sensor_timestamp = new Timestamp(System.currentTimeMillis());
		
	}
	public SensorData(UUID sensor_id, float sensor_temp, Timestamp sensor_timestamp) {
		
		this.sensor_id = sensor_id;
		this.sensor_temp = sensor_temp;
		this.sensor_timestamp = sensor_timestamp;

	}
	
	@Override
	public String toString() {
	    return String.format("{\n\"sensor_id\" : \""+ getSensor_id() +"\",\n"
	    					+ " \"sensor_temp\" : "+ getSensor_temp() +",\n"
	    					+ " \"sensor_timestamp\" : \""+ getSensor_timestamp() +"\"\n}");
	  }
}
