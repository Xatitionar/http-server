/**
 * Class that represents all the values that need to be read from the json inside http request body on /insert query and inserted into Cassandra database
 */

package util;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("sensor_info")
public class SensorInfo {
	private final static Logger LOGGER = LoggerFactory.getLogger(SensorInfo.class);
	
	@PrimaryKey private String sensor_MAC;
	private UUID sensor_ID;
	private String sensor_IP;
	private String sensor_Location;
	private String sensor_Description;
	private int sensor_Number;
	
	public String getSensor_MAC() {
		return sensor_MAC;
	}
	public void setSensor_MAC(String sensor_MAC) {
		this.sensor_MAC = sensor_MAC;
	}
	public UUID getSensor_ID() {
		return sensor_ID;
	}
	public void setSensor_ID(UUID sensor_ID) {
		this.sensor_ID = sensor_ID;
	}
	public String getSensor_IP() {
		return sensor_IP;
	}
	public void setSensor_IP(String sensor_IP) {
		this.sensor_IP = sensor_IP;
	}
	public String getSensor_Location() {
		return sensor_Location;
	}
	public void setSensor_Location(String sensor_Location) {
		this.sensor_Location = sensor_Location;
	}
	public String getSensor_Description() {
		return sensor_Description;
	}
	public void setSensor_Description(String sensorDescription) {
		this.sensor_Description = sensorDescription;
	}
	
	public int getSensor_Number() {
		return sensor_Number;
	}
	
	public void setSensor_Number(int sensorNumber) {
		this.sensor_Number = sensorNumber;
	}
	
	public SensorInfo() {
		this.sensor_MAC ="00-00-00-00-00-00";
		this.sensor_ID = UUID.fromString("00000000-0000-0000-0000-000000000000");
		this.sensor_IP = "0.0.0.0";
		this.sensor_Location = "Osijek";
		this.sensor_Description = "Null";
		this.sensor_Number = 0;
	}
	public SensorInfo(String sensor_MAC, UUID sensor_ID, String sensor_IP, String sensor_Location,
			String sensor_Description, int sensor_Number) {
		
		this.sensor_MAC = sensor_MAC;
		this.sensor_ID = sensor_ID;
		this.sensor_IP = sensor_IP;
		this.sensor_Location = sensor_Location;
		this.sensor_Description = sensor_Description;
		this.sensor_Number = sensor_Number;
	}
	
	public void print() {
		LOGGER.info("Sensor values: ");
		LOGGER.info(" ");
		LOGGER.info(getSensor_MAC());
		LOGGER.info(getSensor_ID().toString());
		LOGGER.info(getSensor_IP());
		LOGGER.info(getSensor_Location());
		LOGGER.info(getSensor_Description());
		LOGGER.info(String.valueOf(getSensor_Number()));
		LOGGER.info(" ");
	}
	
	@Override
	public String toString() {
	    return String.format("{\n\"sensor_id\" : \"" + getSensor_ID() + "\",\n"
	    					+ " \"sensor_mac\" : \"" + getSensor_MAC() + "\",\n"
	    					+ " \"sensor_location\" : \"" + getSensor_Location() + "\",\n"
	    					+ " \"sensor_description\" : \"" + getSensor_Description() + "\",\n"
	    					+ " \"sensor_ip\" : \"" + getSensor_IP() + "\",\n"
	    					+ " \"sensor_number\" : \"" + getSensor_Number() +"\"\n}");
	  }
}

