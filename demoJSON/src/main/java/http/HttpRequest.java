/**
 * Class that represents http request structure with respective getters and setters 
 */

package http;

public class HttpRequest extends HttpMessage{
	
	private String body;
	private HttpMethod method;
	private String requestTarget;
	private String originalHttpVersion;
	private HttpVersion bestCompatilbeVersion;
	
	HttpRequest(){
		
	}

	public HttpMethod getMethod() {
		return method;
	}
	
	public String getRequestTarget() {
		return requestTarget;
	}
	
	public String getOriginalHttpVersion() {
		return originalHttpVersion;
	}
	
	public HttpVersion getBestCompatibleHttpVersion() {
		return bestCompatilbeVersion;
	}

	public void setMethod(String methodName) throws HttpParsingException {
		for(HttpMethod method: HttpMethod.values()) {
			if(methodName.equals(method.name())) {
				this.method = method;
				return;
			}
		}
		throw new HttpParsingException(
				HttpStatusCode.SERVER_ERROR_501_NOT_IMPLEMENTED
		);
	}

	void setRequestTarget(String requestTarget) throws HttpParsingException {
		if(requestTarget == null || requestTarget.length() == 0) {
			throw new HttpParsingException(HttpStatusCode.SERVER_ERROR_500_INTERNAL_SERVER_ERROR);
		}
		this.requestTarget = requestTarget;
		
	}

	
	public void setHttpVersion(String originalHttpVersion) throws BadHttpVersionException, HttpParsingException {
		this.originalHttpVersion = originalHttpVersion;
		this.bestCompatilbeVersion = HttpVersion.getBestCompatibleVersion(originalHttpVersion);
		
		if(this.bestCompatilbeVersion == null) {
			throw new HttpParsingException(
					HttpStatusCode.SERVER_ERROR_505_HTTP_VERSION_NOT_SUPPORTED
					);
		}
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public String toString() {
		return "HttpRequest [body=" + body + ", method=" + method + ", requestTarget=" + requestTarget
				+ ", originalHttpVersion=" + originalHttpVersion + ", bestCompatilbeVersion=" + bestCompatilbeVersion
				+ "]";
	}

}
