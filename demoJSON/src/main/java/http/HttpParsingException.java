/**
 * Exception made for throwing errors while in process of parsing http request
 */

package http;

public class HttpParsingException extends Exception {
	
	private HttpStatusCode errorCode;
	
	public HttpParsingException(HttpStatusCode errorCode) {
		super(errorCode.MESSAGE);
		this.errorCode = errorCode;
	}
	
	public HttpStatusCode getErrorCode() {
		return errorCode;
	}
}
