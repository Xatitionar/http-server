/**
 * Enum for including parsable http method types
 * 
 * Has a guard for max http method length based on parsable methods
 */

package http;

public enum HttpMethod {
	GET, POST, OPTIONS;
	
	public static final int MAX_LENGHT;
	
	static {
		int tempMaxLenght = -1;
		for (HttpMethod method:values()) {
			if(method.name().length() > tempMaxLenght) {
				tempMaxLenght = method.name().length();
			}
		}
		MAX_LENGHT = tempMaxLenght;
	}
}
