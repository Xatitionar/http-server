/**
 * Class that is used for parsing http request into its components
 * 
 * Structure of any http request is request line, headers, body - this class parses the request line and body while leaving headers unparsed since we dont need them
 * 
 */

package http;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpParser {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(HttpParser.class);
	
	private static final int SP = 0x20; // 32
	private static final int CR = 0x0D; // 13
	private static final int LF = 0x0A; // 10
	
	public HttpRequest parseHttpRequest(InputStream inputStream) throws HttpParsingException {
		InputStreamReader reader = new InputStreamReader(inputStream, StandardCharsets.US_ASCII);
		
		HttpRequest request = new HttpRequest();
		HttpMethod method = HttpMethod.OPTIONS;
		String update = "/update";
		String insert = "/insert";
		String delete = "/delete";
		
		try {
			parseRequestLine(reader, request);
			if(!(method.equals(request.getMethod()) &&
					(update.equals(request.getRequestTarget()) ||
					insert.equals(request.getRequestTarget()) ||
					delete.equals(request.getRequestTarget() )))) 
			{
				parseBody(reader, request);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return request;
	}
	
	private void parseRequestLine(InputStreamReader reader, HttpRequest request) throws IOException, HttpParsingException {
		
		boolean methodParsed = false;
		boolean requestTargetParsed = false;
		
		StringBuilder processingDataBuffer = new StringBuilder();
		int _byte;
		while((_byte = reader.read()) >= 0) {
			
			if(_byte == CR) {
				
				_byte = reader.read();
				if(_byte == LF) {
					LOGGER.debug("Request line VERSION to process: {}", processingDataBuffer.toString());
					if(!methodParsed || !requestTargetParsed) {
						throw new HttpParsingException(HttpStatusCode.CLIENT_ERROR_400_BAD_REQUEST);
					}
					
					try {
						request.setHttpVersion(processingDataBuffer.toString());
					} catch (BadHttpVersionException e) {
						throw new HttpParsingException(HttpStatusCode.CLIENT_ERROR_400_BAD_REQUEST);
					}
					
					return;
				}else {
					throw new HttpParsingException(HttpStatusCode.CLIENT_ERROR_400_BAD_REQUEST);
				}
			}
			
			if(_byte == SP) {
				
				if(!methodParsed) {
					LOGGER.debug("Request line METHOD to process: {}", processingDataBuffer.toString());
					request.setMethod(processingDataBuffer.toString());
					methodParsed = true;
				}else if(!requestTargetParsed) {
					LOGGER.debug("Request line TARGET to process: {}", processingDataBuffer.toString());
					request.setRequestTarget(processingDataBuffer.toString());
					requestTargetParsed = true;
				}else {
					throw new HttpParsingException(HttpStatusCode.CLIENT_ERROR_400_BAD_REQUEST);
				}

				processingDataBuffer.delete(0,  processingDataBuffer.length());
			}else {
				processingDataBuffer.append((char)_byte);
				if(!methodParsed) {
					if(processingDataBuffer.length() > HttpMethod.MAX_LENGHT) {
						throw new HttpParsingException(HttpStatusCode.SERVER_ERROR_501_NOT_IMPLEMENTED);
					}
				}
			}
		}
	}
	
	private void parseHeaders(InputStreamReader reader, HttpRequest request) {
		// didn't need to implement, hence empty 
	}
	
	private void parseBody(InputStreamReader reader, HttpRequest request) throws IOException {
		StringBuilder processingBodyBuffer = new StringBuilder();
		int _byte;
		int parenthesisCounter = 0;
		int isBodyFlag = 0;
		
		if((_byte = reader.read()) == 0) {
			request.setBody("");
		}
		else {
			
			while((_byte = reader.read()) >= 0) {
				if(request.getMethod().equals((HttpMethod.GET))) {
					request.setBody(" ");
					break;
				}
				if(_byte == '{') {
					isBodyFlag = 1;
					parenthesisCounter++;
					processingBodyBuffer.append((char)_byte);
				}else if(_byte == '}') {
					parenthesisCounter--;
					processingBodyBuffer.append((char)_byte);
					
					if(parenthesisCounter == 0) {
						isBodyFlag = 0;
						processingBodyBuffer.append((char)_byte);
						request.setBody(processingBodyBuffer.toString());
						break;
					}
				}else {
					if(isBodyFlag == 1) {
						processingBodyBuffer.append((char)_byte);
					}
				}
			}
		}
	}


}
