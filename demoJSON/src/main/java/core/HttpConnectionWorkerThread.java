/**
 * Class that represents each connection on to the server - called by ServerListenerThread
 * 
 * It parses the http request and creates a response based on request target
 * 
 * Request targets are divide on to:
 * 		- /query - basic query which returns a green screen if request is made from a browser, and temperature-date pairs formed into a json if
 * 		   request is made from other sources - need json with "time" and "id" of the sensor in the body of request
 * 		- /insert - insert query which inserts parameters given in http request body into the database - need fully formated json in body of request
 * 		- /queryinfo - query that returns all the stored sensors information data in the database in a json format - no body in request
 * 		- /search - query made from Grafana which returns available variables that Grafana can expect from /query response - no body in request
 * 		- /update - query made for updating sensor info from sensor info table on the webpage by sending all the info of a sensor in body of request
 * 		- /delete - query made for deleteing sensor data and info by sending sensor id in body of request
 */

package core;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.cassandra.core.CassandraOperations;

import com.datastax.oss.driver.api.querybuilder.select.SelectFrom;
import com.example.demo.SingletonConnection;
import com.uwyn.jhighlight.fastutil.chars.CharCollection;

import http.HttpMethod;
import http.HttpParser;
import http.HttpRequest;
import util.ParseSensorInfo;
import util.ParsedRequestJson;
import util.SensorInfo;
import util.Sensor_id;

public class HttpConnectionWorkerThread extends Thread{
	private final static Logger LOGGER = LoggerFactory.getLogger(HttpConnectionWorkerThread.class);
	
	private final static String baseTarget = "/";
	private final static String insertTarget = "/insert";
	private final static String queryTarget = "/query";
	private final static String queryDataTarget = "/querydata";
	private final static String queryInfoTarget = "/queryinfo";
	private final static String searchTarget = "/search";
	private final static String updateTarget = "/update";
	private final static String deleteTarget = "/delete";
	
	private final static String time1h = "timeframe-1h";
	private final static String time1d = "timeframe-1d";
	private final static String time7d = "timeframe-1w";
	
	private final static long lastHour = 3600000;
	private final static long last1day = 86400000;
	private final static long last7days = 604800000;
	
	public HttpParser httpParser;
	
	private Socket socket;
	
	public HttpConnectionWorkerThread(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void run() {
		HttpRequest httpRequest = null;
		InputStream inputStream = null;
		OutputStream outputStream = null;
		
		httpParser = new HttpParser();	

		try {	
			inputStream = socket.getInputStream();
			outputStream = socket.getOutputStream();

			httpRequest = httpParser.parseHttpRequest(inputStream);
			String requestTarget = httpRequest.getRequestTarget();

			SingletonConnection conn = SingletonConnection.getInstance();
			CassandraOperations template = conn.getTemplate();
			
			if(requestTarget != null) {
				//LOGGER.info("Query target: " + requestTarget + ", method: " + httpRequest.getMethod().toString());
				if(queryTarget.equals(requestTarget)){		
					String selectedData = "";
					String body = httpRequest.getBody();
					body = body.substring(0, body.length()-1);
					List<UUID> id = new ArrayList<UUID>(); 
					String time = "";
			
					String preparedBody = ParsedRequestJson.prepareRequestBody(body);
					
					id = ParsedRequestJson.parseID(preparedBody);
					time = ParsedRequestJson.parseTime(preparedBody);
					
					if(time1h.equals(time)) {
						selectedData = SelectFromDatabase.select(template, lastHour, id).toString(); 
					}else if(time1d.equals(time)) {
						selectedData = SelectFromDatabase.select(template, last1day, id).toString(); 
					}else if(time7d.equals(time)) {
						selectedData = SelectFromDatabase.select(template, last7days, id).toString(); 
					}

					if(selectedData == "") {
						OutputResponse.sendResponse(outputStream);
					}else {
						OutputResponse.sendResponse(outputStream, selectedData);	
					}
					
				}
				else if(queryDataTarget.equals(requestTarget)){
					String data = "data";
					
					String selectedDataInternal = SelectFromDatabase.select(template, data).toString();

					OutputResponse.sendResponse(outputStream, selectedDataInternal);
				}
				else if (insertTarget.equals(requestTarget)){
					String bodyInternal = httpRequest.getBody();
					HttpMethod method = HttpMethod.OPTIONS;
					SensorInfo sensorInfo;
					
					if(httpRequest.getMethod().equals(method)) {
						String responseOK = "cors_response";
						
						OutputResponse.sendResponse(outputStream, responseOK);
					}else {
						if(bodyInternal.contains("}}")) {
							bodyInternal = bodyInternal.substring(bodyInternal.indexOf("{"), bodyInternal.indexOf("}}") + 1);
						}
						sensorInfo = ParseSensorInfo.parse(bodyInternal);
						
						template.insert(sensorInfo);

						OutputResponse.sendResponse(outputStream, sensorInfo);
					}

				}else if (deleteTarget.equals(requestTarget)){
					String body = httpRequest.getBody();
					HttpMethod method = HttpMethod.OPTIONS;
					SensorInfo sensorInfo;
					
					if(httpRequest.getMethod().equals(method)) {
						String responseOK = "cors_response";
						
						OutputResponse.sendResponse(outputStream, responseOK);
					}else {
						if(body.contains("}}")) {
							body = body.substring(body.indexOf("{"), body.indexOf("}}") + 1);
						}
						
						sensorInfo = ParseSensorInfo.parse(body);
						
						List<UUID> id = new ArrayList<UUID>();
						id.add(sensorInfo.getSensor_ID());
						
						SelectFromDatabase.delete(template, id);
						
						OutputResponse.sendResponse(outputStream, sensorInfo);					
					}


				}else if(queryInfoTarget.equals(requestTarget)) {
					String info = "info";
					
					String selectedDataInternal = SelectFromDatabase.select(template, info).toString();

					OutputResponse.sendResponse(outputStream, selectedDataInternal);

				}else if(baseTarget.equals(requestTarget)) {
					String responseOK = "test_response";

					OutputResponse.sendResponse(outputStream, responseOK);

				}else if(searchTarget.equals(requestTarget)) {
						
						String chooser = "idFromInfo";
						
						String response = SelectFromDatabase.select(template, chooser).toString();
						
						OutputResponse.sendResponse(outputStream, response);
					
				}else if(updateTarget.equals(requestTarget)) {
					HttpMethod method = HttpMethod.OPTIONS;
					
					if(method.equals(httpRequest.getMethod())) {
						String responseOK = "cors_response";
						OutputResponse.sendResponse(outputStream, responseOK);
					}else {
						String body = httpRequest.getBody();
						if(body.contains("}}")) {
							body = body.substring(body.indexOf("{"), body.indexOf("}}") + 1);
						}
						
						SensorInfo sensorInfoReceived = ParseSensorInfo.parse(body);	
						
						SelectFromDatabase.update(template, sensorInfoReceived);
						
						OutputResponse.sendResponse(outputStream, sensorInfoReceived);

					}							
				}else if(deleteTarget.equals(requestTarget)) {
					String body = httpRequest.getBody();
					List<UUID> id = new ArrayList<UUID>();
					id.addAll(ParsedRequestJson.parseID(body));
					
					SelectFromDatabase.delete(template, id);
					
					String responseOK = "Successfuly deleted " + id + "!";
					
					OutputResponse.sendResponse(outputStream, responseOK);
				}
			}
			//LOGGER.info("Processing finished");
		} catch (Exception e) {
			LOGGER.error("Problem with communication", e);
			e.printStackTrace();
		} finally {
			if(inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {}
			}
			
			if(outputStream != null) {
				try {
					outputStream.close();
				} catch (IOException e) {}
			}

			if(socket != null) {
				try {
					socket.close();
				} catch (IOException e) {}
			}
		}
		
	}
}
