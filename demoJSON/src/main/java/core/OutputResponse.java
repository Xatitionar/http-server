/**
 * Class that outputs the http response back to the query maker
 * 
 * Has two overloaded functions - one parameter variant returns a green screen in http body (browser request), while
 * two parameter variant returns an 200 OK message for connection testing purposes and data returned from the database for specific queries
 */

package core;

import java.io.IOException;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import util.SensorInfo;

public class OutputResponse {
	private final static Logger LOGGER = LoggerFactory.getLogger(HttpConnectionWorkerThread.class);
	
	final static String CRLF = "\r\n";
	final static String jsonFromList = "<html><head><title>Green screen</title></head><body style=\"background-color:green;\"><h1>Green screen means this server workin</h1></body></html>";
	final static String testResponse = "test_response";
	final static String CorsIPResponse = "http://192.168.137.115:4200";
	
	public static void sendResponse(OutputStream outputStream) {
		
		String response = 
				"HTTP/1.1 200 OK" + CRLF +
				CRLF + 
				jsonFromList +
				CRLF + CRLF;
		
		try {
			outputStream.write(response.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void sendResponse(OutputStream outputStream, String jsonFromListInput) {

		String response;

		if(testResponse.equals(jsonFromListInput)) {
			response = "HTTP/1.1 200 OK" + CRLF +
					CRLF;
		}else if(jsonFromListInput.equals("cors_response")){
			response = "HTTP/1.1 200 OK" + CRLF +
					"Access-Control-Allow-Credentials: true" + CRLF +
					"Access-Control-Allow-Origin: " + CorsIPResponse + CRLF +
					"Vary: origin" + CRLF +
					"Access-Control-Allow-Methods: POST, GET, PUT" + CRLF +
					"Access-Control-Allow-Headers: content-type" + CRLF +
					CRLF;
		}else if(jsonFromListInput.contains("[")) {
			response = "HTTP/1.1 200 OK" + CRLF +
					CRLF + 
					jsonFromListInput +
					CRLF + CRLF; 
		}else {
			response = "HTTP/1.1 200 OK" + CRLF +
					CRLF + 
					"[\n" + jsonFromListInput + "\n]" +
					CRLF + CRLF;
		}

		try {
			outputStream.write(response.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void sendResponse(OutputStream outputStream, SensorInfo sensorInfo) {
		String response = "HTTP/1.1 200 OK" + CRLF +
						CRLF + 
						sensorInfo.toString() +
						CRLF + CRLF; 
		
		try {
			outputStream.write(response.getBytes());
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}
