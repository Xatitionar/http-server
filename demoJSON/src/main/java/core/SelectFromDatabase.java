/**
 * Class used for fetching data from Cassandra database
 * 
 * Has two overloaded functions - one parameter variant returns all the sensors information data in a from of a json, while
 * three parameter variant returns temperature points for requested sensor and timeframe also in a json format 
 */

package core;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.core.query.Criteria;
import org.springframework.data.cassandra.core.query.Query;
import org.springframework.data.cassandra.core.query.Update;

import com.example.demo.JSONSensor;

import util.SensorData;
import util.SensorInfo;
import util.Sensor_temp;
import util.Sensor_timestamp;

public class SelectFromDatabase {
	private final static Logger LOGGER = LoggerFactory.getLogger(SelectFromDatabase.class);
		
	public static StringBuilder select(CassandraOperations template, long date, List<UUID> id) {
		StringBuilder stringbuilder = new StringBuilder();
		List<Sensor_temp> allTempValues;
		List<Sensor_timestamp> allTimestampValues;
		
		stringbuilder.append("[\n\t");
		
		for(int i = 0; i < id.size(); i++) {
			allTempValues = template.select("select sensor_temp from sensor_values WHERE sensor_timestamp >"
					+ (System.currentTimeMillis() - date) + " AND sensor_id = " + id.get(i) + " ALLOW FILTERING",
					Sensor_temp.class);
			
			allTimestampValues = template.select(
					"select sensor_timestamp from sensor_values WHERE sensor_timestamp >"
							+ (System.currentTimeMillis() - date) + " AND sensor_id = " + id.get(i) + " ALLOW FILTERING",
					Sensor_timestamp.class);
			
			stringbuilder.append("{\n\t\"target\":\"" + id.get(i).toString() + "\",\n\t\"datapoints\":[\n\t\t");

			for (int j = 0; j < allTempValues.size(); j++) {
				stringbuilder.append("[");
				stringbuilder.append(allTempValues.get(j));
				stringbuilder.append(", ");
				stringbuilder.append(allTimestampValues.get(j).convertToMilis());
				stringbuilder.append("]");
				if ((allTempValues.size() - 1) == j) {

				} else {
					stringbuilder.append(",\n\t\t");
				}
			}
			if(i == id.size() - 1) {
				stringbuilder.append("\n\t]\n\t}");
			}else {
				stringbuilder.append("\n\t]\n\t},\n");
			}

		}

		stringbuilder.append("\n]");
	    return stringbuilder;
	}
		
	public static StringBuilder select(CassandraOperations template, String tableChooser) {
		StringBuilder stringbuilder = new StringBuilder();
		String data = "data";
		String idFromInfo = "idFromInfo";
		
		if(data.equals(tableChooser)) {
			List<SensorData> allValues = template.select("select * from sensor_values", SensorData.class);
			
		    for(int i = 0; i < allValues.size(); i++) {
		    	stringbuilder.append(allValues.get(i));
		    	if((allValues.size()-1) == i) {
		    		
		    	}else {
		    		stringbuilder.append(",\n");
		    	}
		    }
		}else if(idFromInfo.equals(tableChooser)) {
			List<SensorInfo> allValues = template.select("select * from sensor_info", SensorInfo.class);
			
			
			stringbuilder.append("[");
			
			for(int i = 0; i < allValues.size(); i++) {
				stringbuilder.append("\"");
				stringbuilder.append(allValues.get(i).getSensor_ID().toString());
		    	if((allValues.size()-1) == i) {
		    		
		    	}else {
		    		stringbuilder.append("\", ");
		    	}
		    }
			
			stringbuilder.append("\"]");
			
		}else {
			List<SensorInfo> allValues = template.select("select * from sensor_info", SensorInfo.class);
			
		    for(int i = 0; i < allValues.size(); i++) {
		    	stringbuilder.append(allValues.get(i));
		    	if((allValues.size()-1) == i) {
		    		
		    	}else {
		    		stringbuilder.append(",\n");
		    	}
		    }
		}
		
	    return stringbuilder;
	}
	
	public static void delete(CassandraOperations template, List<UUID> uuid) {
		List<SensorInfo> sensorList = template.select("SELECT * FROM sensor_info WHERE sensor_id = " + uuid.get(1) + "ALLOW FILTERING", SensorInfo.class);
		SensorInfo sensor = sensorList.get(0);
		
		template.deleteById(sensor.getSensor_MAC(), SensorInfo.class);			
		template.deleteById(uuid, JSONSensor.class);
	}
	
	public static void update(CassandraOperations template, SensorInfo received) {
		Update update = Update.empty().set("sensor_id", received.getSensor_ID())
				.set("sensor_location", received.getSensor_Location())
				.set("sensor_description", received.getSensor_Description())
				.set("sensor_ip", received.getSensor_IP());
		
		template.update(Query.query(Criteria.where("sensor_mac").is(received.getSensor_MAC())), update, SensorInfo.class);
	}
}
