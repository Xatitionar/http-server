/**
 * Class made for fetching a configuration (port and webroot) for the api
 * 
 * It parses the given file, which needs to be a json, and stores it as a Configuration class object
 * 
 */

package config;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;

import util.Json;

public class ConfigurationManager {

	private static ConfigurationManager myConfigurationManager;
	private static Configuration myCurrentConfiguration;

	private ConfigurationManager() {

	}

	public static ConfigurationManager getInstance() {
		if (myConfigurationManager == null) {
			myConfigurationManager = new ConfigurationManager();
		}
		return myConfigurationManager;
	}

	public void loadConfigurationFile(String filePath) {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filePath);

		StringBuffer sb = new StringBuffer();
		int i;

		try {
			while ((i = inputStream.read()) != -1) {
				sb.append((char) i);
			}
			
			inputStream.close();
		} catch (IOException e) {
			throw new HttpConfigurationException(e);
		}
		
		JsonNode conf;
		try {
			conf = Json.parse(sb.toString());
		} catch (IOException e) {
			throw new HttpConfigurationException("Error parsing configuration file", e);
		}
		
		try {
			myCurrentConfiguration = Json.fromJson(conf, Configuration.class);
		} catch (JsonProcessingException e) {
			throw new HttpConfigurationException("Error parsing configuration file, internal", e);
		}

	}

	public Configuration getCurrentConfiguration() {
		if (myCurrentConfiguration == null) {
			throw new HttpConfigurationException("No current configuration set");
		}
		return myCurrentConfiguration;
	}

}
