/**
 * Exception class for different types of error handling
 */

package config;

public class HttpConfigurationException extends RuntimeException{

	public HttpConfigurationException() {

	}

	public HttpConfigurationException(String message, Throwable cause) {

	}

	public HttpConfigurationException(String message) {

	}

	public HttpConfigurationException(Throwable cause) {

	}
}
